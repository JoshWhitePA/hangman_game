package Hman;

import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
/************************************************************/
/* Author: Joshua White */
/* Creation Date: April 20, 2015 */
/* Due Date: April 30, 2015 */
/* Course: CSC243 010 */
/* Professor Name: Randy Kaplan */
/* Assignment: #3 */
/* Filename: GUI.java */
/* Purpose: This program is to present a player a random word from a word list */
/* and allow them to guess the correct letters in the form of a hangman game */
/* the correct word will be displayed if the word is wrong*/
/************************************************************/

public class GUI extends JFrame implements WindowListener, ActionListener{
	/**
	 * Beginning of class member declaration
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton button;
	ArrayList<JButton> buttonArray = new ArrayList<JButton>();
	Puzzle puzzle = new Puzzle();//instance of the puzzle class where info is grabbed from the words class
	JPanel panel;//All labels and buttons are on this jpanel
	JLabel blankedWord;//jlabel that displays the text of the blanked out word and is updated with correct letters
	JLabel messageToPlayer;//displays instructions to the player
	JLabel type;//displays the type of word defined in the word object
	JLabel hangmanImage;//displays hangman images
	JLabel winOrLose;//displays message if the player wins or loses


	JButton Start;
	JButton NewWord;
	JButton ShowAnswer;
	JButton Quit;
	ArrayList<JLabel> underscoreArray = new ArrayList<JLabel>();//allows me to set the visibility of the underscoreimages created in the
	//for loop
	JLabel underscore;//holds the underscore images
	
	WordSpaced wsp = new WordSpaced();//polymorphism/inheritance example, extends Labels class
	Instructions instruct = new Instructions();//polymorphism/inheritance example, extends Labels class
	
	
	public GUI(String s){
		super(s);
		panel = new JPanel();//creates the jpanel
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);//told not to use a layout manager
		setSize(1000,700);//JFrame size 
		setResizable(false);//user cannot resize the window
		panel.setBounds(0,0,1000,700);//creates jpanel bounds
		addWindowListener(this);
		puzzle.setCorrectLetterCount(0);//sets the correct letter count to zero
		char letter = 'A';//used in the letter button creation for loop
		int xLocation = 60; 
		int yLocation = 60;
		puzzle.resetNumberWrong();//sets number wrong to zero

		for (int row = 0;row < 2; row++){ // allows there to be two rows of keys
			for (int kerning = 0; kerning < 13; kerning++){ //makes keys in rows of 13
				if (letter == '[' ){
						letter++;
						break;
						//if for some reason incremented got screwed up it would stop at the [ which is after z
					}
				String path = Character.toString(letter).toLowerCase();//makes the letter lowercase for finding resource
				path = "letters/"+ path + ".png";//sets the name of image based on letter value
				ImageIcon ico = new ImageIcon(getClass().getResource(path));//gets the image
				button = new JButton(Character.toString(letter).toLowerCase(),ico);//creates a jbutton the the icon and text of letter value
				button.setBounds(120 + (xLocation * kerning), 458 + (yLocation * row),40,40);//location and size of button
				button.setBorder(BorderFactory.createEmptyBorder());//allows the image to act as the button
				button.setContentAreaFilled(false);//allows the image to act as the button
				button.setEnabled(false);//disables initially so start can enable them
				this.add(button);//adds button to frame
				buttonArray.add(button);//add the button to array list to setdisbled property
				button.addActionListener(this);//gives the button an actionlistioner
			
				letter++;//increments letter value to go to next letter
			}
		}
		
		JLabel heading = new JLabel("The Game Of Hangman");
		Font headingFont = new Font("Serif", Font.BOLD, 30);
		heading.setBounds(300,10,700,80);
		heading.setFont(headingFont);
		add(heading);
		
		ImageIcon icon = new ImageIcon(getClass().getResource("letters/underscore.png"));
		int locationx = 150;//initial x location, grows as more underscores are added
		//I could have made it so that the number of underscores would grow if a word was too long but it would mess with my hangman image
		//or just run out of space
		for(int idx = 0; idx < /*puzzle.getBlankedWord().length()*/ 12;idx++){
			underscore = new JLabel(icon);//creates jlabel with underscore image
			underscore.setBounds(locationx,309,25,10);//changes the x position each time the list is run through
			underscore.setVisible(false);//sets the default visibility to false
			if(idx < puzzle.getBlankedWord().length()){
				underscore.setVisible(true);//makes the underscores visible if it within the size of the first word chosen
			}
			underscoreArray.add(underscore);//adds underscore labels to array list so they can be referred to later
			add(underscore);//adds the jframe
			locationx +=46; //increments the xlocation
		}
		
		blankedWord = new JLabel();//label for displaying the word
		blankedWord.setText(wsp.returnFormated(puzzle.getBlankedWord()));//returns formatted text with spaces and puts them in label
		Font labelFont = new Font("Lucida Console", Font.BOLD, 39);//sets the font to a monospaced font
		blankedWord.setFont(labelFont);//changes the font of the label
		blankedWord.setBounds(150,250,520,80);
		add(blankedWord);//add it to jframe
		
		instruct.setStrang("<html>You will be shown space holders for a word. Guess one letter at a time from the<br/>alphabet and see if you can guess the word.<html");
		//above uses default method from Labels to display text;
		Font messageFont = new Font("Serif", Font.BOLD, 20);
		messageToPlayer = new JLabel(instruct.returnFormated());
		messageToPlayer.setFont(messageFont);
		messageToPlayer.setBounds(120,100,720,80);
		messageToPlayer.setVisible(true);
		add(messageToPlayer);

		//shows type of word the word is
		Font typeOf = new Font("Serif", Font.BOLD, 30);
		type = new JLabel("The word Type is a: "+puzzle.words.myWord.getType());//word class has the type definition
		type.setFont(typeOf);
		type.setBounds(120,380,420,80);
		type.setVisible(false);
		add(type);
		
		//displays message based on weather the player wins or loses
		winOrLose = new JLabel();
		Font win_lose = new Font("Serif", Font.BOLD, 40);
		winOrLose.setFont(win_lose);
		winOrLose.setBounds(450,175,420,80);
		winOrLose.setForeground(Color.RED);
		add(winOrLose);
		
		//displays hangman image
		ImageIcon hangICO = new ImageIcon(getClass().getResource("letters/1.png"));//sets the blank gallows as the image
		hangmanImage = new JLabel(hangICO);
		hangmanImage.setBounds(700, 175, 176, 254);
		add(hangmanImage);
		
		//control button creation an instantiation
		 Start = new JButton("Start");
		 NewWord = new JButton("New Word");
		 ShowAnswer = new JButton("Show Answer");
		 Quit = new JButton("Quit");
		
		Start.setBounds(100,600,170,40);
		add(Start);
		Start.addActionListener(this);
		
		
		NewWord.setBounds(270,600,170,40);
		NewWord.setEnabled(false);
		add(NewWord);
		NewWord.addActionListener(this);
		
		
		ShowAnswer.setBounds(440,600,170,40);
		ShowAnswer.setEnabled(false);
		add(ShowAnswer);
		ShowAnswer.addActionListener(this);
		
	
		Quit.setBounds(610,600,170,40);
		add(Quit);
		Quit.addActionListener(this);
		
		//adds the jpanel to the jframe
		add(panel);
		setVisible(true);
	}
	
	/*private void displayTodLabel(String infoToDiplay){
		char[] stringtoChars = infoToDiplay.toCharArray();
		String display = "";
		for(int i = 0; i < infoToDiplay.length(); i++){
			display += stringtoChars[i] + " ";
		}
		System.out.println("inside displayLabel: "+display);
		blankedWord.setText(display);
	}*/

	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		//system out are for debugging
		
		ArrayList<Integer> array = new ArrayList<Integer>();//holds the return from the puzzle.checkLetter
		ImageIcon blankHangmanImage = new ImageIcon(getClass().getResource("letters/1.png"));//creates local hanmanimage icon

		
		//System.out.println("Worked" + ((JButton)e.getSource()).getActionCommand());
		
		if (((JButton)e.getSource()).getActionCommand() == "Start"){
			//if the start button is pressed
			enableAllLetterButtons();
			type.setVisible(true);//set type label to visible
			//enable disable buttons 
			Start.setEnabled(false);
			NewWord.setEnabled(true);
			ShowAnswer.setEnabled(true);
		
			//System.out.println("inStart");
			
		}
		else if(((JButton)e.getSource()).getActionCommand() == "New Word"){
			//if new word button is pressed
			
			//System.out.println("in Create Word");
			
			puzzle.createNewWord();//pulls random word from text file and sets the word and type members
			winOrLose.setText("");//makes the text of the win lose label to nothing
			blankedWord.setText(wsp.returnFormated(puzzle.getBlankedWord()));///used to display word with spaces, word will be blanked out
			type.setText("The word Type is a: "+puzzle.words.myWord.getType());//changes the text of the type label
			enableAllLetterButtons();//resets keyboard
			//enable disable buttons
			ShowAnswer.setEnabled(true);
			Start.setEnabled(false);
			
			makeUnderScoresGone();//makes all underscore jlables invisible
			makeUnderScoresVisible( puzzle.words.myWord.getWord().length());//makes the number of jlabels required for 
			hangmanImage.setIcon(blankHangmanImage);//makes hangman image the default one
			puzzle.resetNumberWrong();
			ImageIcon icon = new ImageIcon(getClass().getResource("letters/underscore.png"));
			int locationx = 269;
			
			makeUnderScoresGone();
			makeUnderScoresVisible(puzzle.getBlankedWord().length());
			//commented code could be used if word size was more than I chose, would be within an if checking size of the array list
			/*for(int idx = 0; idx < puzzle.getBlankedWord().length();idx++){
				
				underscore = new JLabel(icon);
				underscore.setBounds(locationx,309,25,10);
				underscore.setVisible(true);
				add(underscore);
				locationx +=46;
			}*/
			puzzle.resetNumberWrong();
			puzzle.setCorrectLetterCount(0);
		}
		else if(((JButton)e.getSource()).getActionCommand() == "Show Answer"){
			//shows the correct word 
			winOrLose.setText("You Lose!");
			blankedWord.setText(wsp.returnFormated(puzzle.getRightWord()));//
			disableAllLetterButtons();
			ShowAnswer.setEnabled(false);
			puzzle.resetNumberWrong();
			//System.out.println("in ShowAnswer");
		}
		else if(((JButton)e.getSource()).getActionCommand() == "Quit"){
			//System.out.println("in Quit");
			//closes application
			super.dispose();
		}
		else{
			//if none of the above cases are met then the event must be a letter button
			array = puzzle.checkLetter(((JButton)e.getSource()).getActionCommand());//searches through word,returns array of indexes
			//based on the name of the button
			puzzle.addCorrectLetterCount(array.size());//if the letter is found the array will be populated 
			//the array hold the index location on each of the correct letters
			if (array.isEmpty()){
				//wrong letter, post new hangman image,add to number wrong
				ImageIcon newICO = new ImageIcon(getClass().getResource("letters/"  + (puzzle.getNumberWrong()+2) +".png"));//each image will be iterated based
				//on hom man letters they have wrong
				hangmanImage.setIcon(newICO);
				puzzle.setNumberWrong(1);
				
				if (puzzle.getNumberWrong() >= Puzzle.youRDone){
					//checked if the number wrong is more than static number that would mean the person lost
					blankedWord.setText(wsp.returnFormated(puzzle.getRightWord()));//
					disableAllLetterButtons();
					winOrLose.setText("You Lose!");
					ShowAnswer.setEnabled(false);
					puzzle.resetNumberWrong();
					//disable all letter buttons and 
				}
				((JButton)e.getSource()).setEnabled(false);//makes that letter button disabled
			}
			else{
				//the letter was correct
				blankedWord.setText(wsp.returnFormated(puzzle.charReplacement(array)));//replace the characters using the array
				//that returns the corrected word and it is sent to the formatter for the jlabel
				((JButton)e.getSource()).setEnabled(false);//disables letter button
				if (puzzle.getCorrectLetterCount() == puzzle.getRightWord().length()){
					//checks if I have chosen all of the letters
					winOrLose.setText("You Win!");
					puzzle.setCorrectLetterCount(0);
					ShowAnswer.setEnabled(false);
				}
				else{
					winOrLose.setText("");
				}
			}	
			
		}
		try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//System.out.println("Get number right: "+puzzle.getCorrectLetterCount());
		
	}
	
	//enables all letterbuttons using the array they were put into when they were crated
	public void enableAllLetterButtons(){
		for(JButton buttons : buttonArray )
		    buttons.setEnabled(true);
		
		try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	//disables all letter buttons
	public void disableAllLetterButtons(){
		for(JButton buttons : buttonArray )
		    buttons.setEnabled(false);
	}

	//makes all underscores invisible
	public void makeUnderScoresGone(){
		for (JLabel underscores : underscoreArray)
			underscores.setVisible(false);
	}
	
	//makes underscores all visible
	public void makeUnderScoresVisible( int size){
		for (int idx = 0;idx < size; idx++){
			underscoreArray.get(idx).setVisible(true);
		}
	}
	
	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		super.dispose();
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		super.dispose();
		System.exit(0);
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	/*public void setMessage(String message){
		this.message = message;
	}
	public String getMessage(){
		return message;
	}*/

}
