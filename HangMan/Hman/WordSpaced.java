package Hman;
/************************************************************/
/* Author: Joshua White */
/* Creation Date: April 20, 2015 */
/* Due Date: April 30, 2015 */
/* Course: CSC243 010 */
/* Professor Name: Randy Kaplan */
/* Assignment: #3 */
/* Filename: WordSpaced.java */
/* Purpose: This program is to present a player a random word from a word list */
/* and allow them to guess the correct letters in the form of a hangman game */
/* the correct word will be displayed if the word is wrong*/
/************************************************************/
public class WordSpaced extends StringDisplay{
	
	WordSpaced(){
		super();
	}
	
	//adds spaces in between letters for formatting
	public String returnFormated(String strang){
		char[] stringtoChars = strang.toCharArray();
		String display = "";
		for(int i = 0; i < strang.length(); i++){
			display += stringtoChars[i] + " ";
		}
		setStrang(display);
		return display;
	}
	
}
