package Hman;
/************************************************************/
/* Author: Joshua White */
/* Creation Date: April 20, 2015 */
/* Due Date: April 30, 2015 */
/* Course: CSC243 010 */
/* Professor Name: Randy Kaplan */
/* Assignment: #3 */
/* Filename: StringDisplay.java */
/* Purpose: This program is to present a player a random word from a word list */
/* and allow them to guess the correct letters in the form of a hangman game */
/* the correct word will be displayed if the word is wrong*/
/************************************************************/
public class StringDisplay {
	private String strang;
	StringDisplay(){
	}

	public String returnFormated(){
		return strang;
	}
	
	
	/**
	 * @return the strang
	 */
	public String getStrang() {
		return strang;
	}
	
	/**
	 * @param strang the strang to set
	 */
	public void setStrang(String strang) {
		this.strang = strang;
	}
	
	
	
}
