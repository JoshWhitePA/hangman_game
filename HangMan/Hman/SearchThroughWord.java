package Hman;
/************************************************************/
/* Author: Joshua White */
/* Creation Date: April 20, 2015 */
/* Due Date: April 30, 2015 */
/* Course: CSC243 010 */
/* Professor Name: Randy Kaplan */
/* Assignment: #3 */
/* Filename: SearchThroughWord.java */
/* Purpose: This program is to present a player a random word from a word list */
/* and allow them to guess the correct letters in the form of a hangman game */
/* the correct word will be displayed if the word is wrong*/
/************************************************************/
import java.util.ArrayList;

public class SearchThroughWord {
	
	
	public ArrayList<Integer> findIndexes(String word, char key){
	String string = word;
	ArrayList<Integer> list = new ArrayList<Integer>();
	char character = key;
	for(int i = 0; i < string.length(); i++){
	    if(string.charAt(i) == character){
	       list.add(i);
	    	}
		}
		return list;
	}
		
	
}
