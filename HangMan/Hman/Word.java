package Hman;

/************************************************************/
/* Author: Joshua White */
/* Creation Date: April 20, 2015 */
/* Due Date: April 30, 2015 */
/* Course: CSC243 010 */
/* Professor Name: Randy Kaplan */
/* Assignment: #3 */
/* Filename: Word.java */
/* Purpose: This program is to present a player a random word from a word list */
/* and allow them to guess the correct letters in the form of a hangman game */
/* the correct word will be displayed if the word is wrong*/
/************************************************************/
public class Word {

private String word;
private String blankedOutWord;
private String type;
private int size;
//private boolean used = false;

public Word (){
	super();
	type = "";
	word ="";
	setInitialBlankedOutWord();
	size = 0;
}

public Word (String aWord)
{
	String[] token;
	token = delimString(aWord);
	word = token[0];
	type = token[1];
	size = token[0].length();
	setInitialBlankedOutWord();

}

public void setWord(String aWord){	
	word = aWord;
	size = word.length();
	setInitialBlankedOutWord();
}

public String getWord(){
	return word;
}

public void setSize(int theSize){//sets the size of the string word
	size = theSize;
}

public int getSize(){//gets the size of the string word
	return size;
}
public String[] delimString(String wordSplit){
	String wordBeingSplit = wordSplit;
	String[] tokens = wordBeingSplit.split("\\|");
	return tokens;
}

/**
 * @return the type
 */
public String getType() {
	return type;
}

/**
 * @param type the type to set
 */
public void setType(String type) {
	this.type = type;
}

/**
 * @return the blankedOutWord
 */
public String getBlankedOutWord() {
	return blankedOutWord;
}

/**
 * @param blankedOutWord the blankedOutWord to set
 */
public void setInitialBlankedOutWord() {
	String blanks = "";
	for (int i = 0; i < word.length(); i++){
		blanks = blanks + " ";
	}
	blankedOutWord = blanks;
}

public void setCurrentBlankedOutWord(String replacement){
	blankedOutWord = replacement;
}

}

