package Hman;

public class Words {
	WordSource wordsource = new WordSource();//
	
	Word myWord;
	
	Words(){
		super();
		createNewWord();//pulls new word out of the list
	}
	
	public String getBlanked(){
		return myWord.getBlankedOutWord();
	}
	public void setBlanked(String newBlanks){
		myWord.setCurrentBlankedOutWord(newBlanks);
	}
	
	;//pulls new line out of the list and sets it to word
	public void createNewWord(){
		Word word = new Word(wordsource.getTheWordFromList());
		myWord = word;	
	}
	
	public String getTheWord(){
		return myWord.getWord();
	}
	
	public String getWordType(){
		return myWord.getType();
	}
	public int getWordLength(){
		return myWord.getSize();
	}
}
