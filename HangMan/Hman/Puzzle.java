package Hman;
/************************************************************/
/* Author: Joshua White */
/* Creation Date: April 20, 2015 */
/* Due Date: April 30, 2015 */
/* Course: CSC243 010 */
/* Professor Name: Randy Kaplan */
/* Assignment: #3 */
/* Filename: Puzzle.java */
/* Purpose: This program is to present a player a random word from a word list */
/* and allow them to guess the correct letters in the form of a hangman game */
/* the correct word will be displayed if the word is wrong*/
/************************************************************/
import java.util.ArrayList;

public class Puzzle {
	Words words = new Words();
	SearchThroughWord search = new SearchThroughWord();
	ArrayList<Integer> array = new ArrayList<Integer>();
	int numberWrong;
	public static int youRDone = 6;
	int correctLetterCount;
	
	public void makeThePuzzle(){
		words.createNewWord();
		for(Integer i : array){
	       // System.out.println(i);
	    }
		
	}
	
	public void createNewWord(){
		words.createNewWord();
	}
	
	public String charReplacement(ArrayList<Integer> array){
		String charsReplaced = "";
		char[] stringtoChars = words.getBlanked().toCharArray();
		for( Integer indexOfCorrectLetters: array){
			//System.out.println("getblanked: "+words.getBlanked());
			//System.out.println("index: "+indexOfCorrectLetters);
			
			stringtoChars[indexOfCorrectLetters] = words.getTheWord().charAt(indexOfCorrectLetters);
			charsReplaced = String.valueOf(stringtoChars);
			}
		words.setBlanked(charsReplaced);
		//System.out.println("getblanked after: "+words.getBlanked());
		return charsReplaced;
	}
	
	
	public ArrayList<Integer> checkLetter(String letterChecked){
		array = search.findIndexes(words.getTheWord(), letterChecked.charAt(0));
		return array;
	}
	
	public void setNumberWrong(int numWrong){
		numberWrong += numWrong;
	}
	public void resetNumberWrong(){
		numberWrong = 0;
	}
	
	public String getRightWord(){
		return words.getTheWord();
	}
	
	public String getBlankedWord(){
		return words.getBlanked();
	}
	
	public int getNumberWrong(){
		return numberWrong;
	}
	
	public int getCorrectLetterCount(){
		return correctLetterCount;
	}
	
	public void setCorrectLetterCount(int count){
		correctLetterCount = count;
	}
	public void addCorrectLetterCount(int size){
		correctLetterCount +=size;
	}
	
	
}
