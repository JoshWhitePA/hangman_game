package Hman;
/************************************************************/
/* Author: Joshua White */
/* Creation Date: April 20, 2015 */
/* Due Date: April 30, 2015 */
/* Course: CSC243 010 */
/* Professor Name: Randy Kaplan */
/* Assignment: #3 */
/* Filename: WordSource.java */
/* Purpose: This program is to present a player a random word from a word list */
/* and allow them to guess the correct letters in the form of a hangman game */
/* the correct word will be displayed if the word is wrong*/
/************************************************************/
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;

public class WordSource {
	
	private String fileName = "/Users/josh/Documents/workspace/HangMan/Hman/words.txt";
	List<String> lines = null;
	
	public String getTheWordFromList(){
		
				try {
					lines = Files.readAllLines(Paths.get(fileName),StandardCharsets.UTF_8);
				} catch (IOException e) {
					System.out.println("File can't be opened.");
				}

				int randomWordIndex = rand(1, lines.size()-2);//-2 in case of the file having extra newline
				//System.out.println(lines.get(randomWordIndex));
				return lines.get(randomWordIndex);//the random word index gets rand line
	}
	//gets random number based on number of lines
	public int rand(int min, int max){
		Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
		
}

