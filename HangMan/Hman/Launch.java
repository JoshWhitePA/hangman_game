package Hman;
/************************************************************/
/* Author: Joshua White */
/* Creation Date: April 20, 2015 */
/* Due Date: April 30, 2015 */
/* Course: CSC243 010 */
/* Professor Name: Randy Kaplan */
/* Assignment: #3 */
/* Filename: Launch.java */
/* Purpose: This program is to present a player a random word from a word list */
/* and allow them to guess the correct letters in the form of a hangman game */
/* the correct word will be displayed if the word is wrong*/
/************************************************************/
public class Launch {
//launches main program
	
	//!!!!!note:file url in WordSource is absolute and needs to be changed to the directory it is in
	public static void main(String []args){
		new GUI("Hangman");
	}
}
